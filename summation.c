///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01d - Summation  
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Jeraldine Milla <millaje@hawaii>
// @date   12 Jan 2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   int n = atoi(argv[1]);
   int sum = 0;                      //initializing 'sum' to 0      
   int i = 1;                        //initializing 'i' with 1
   
   printf("The sum of the digits from %d to %d", i, n);  //print statement from 'i' to 'n'

   for(i = 1; i<=n; i++){            //summing digits from 'i' to 'n'
      sum += i;
   }
   printf(" is %d.\n", sum);         //printing sum
   
   return 0;
}
